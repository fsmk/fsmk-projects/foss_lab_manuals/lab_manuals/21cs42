import java.util.Scanner;

public class DijkstraAlgorithm {
    private static final int MAXNODES = 10;
    private static final int INF = 9999;

    public static void main(String[] args) {
        int n, cost[][] = new int[MAXNODES][MAXNODES], dist[] = new int[MAXNODES], visited[] = new int[MAXNODES], path[] = new int[MAXNODES], i, j, source, dest;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of nodes: ");
        n = scanner.nextInt();

        System.out.println("Enter the Cost Matrix:");
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                cost[i][j] = scanner.nextInt();
            }
        }

        System.out.print("Enter the Source vertex: ");
        source = scanner.nextInt();

        System.out.println("\nShortest paths from Source Vertex " + source + " to other vertices:");

        for (dest = 0; dest < n; dest++) {
            fnDijkstra(cost, dist, path, visited, source, dest, n);

            if (dist[dest] == INF) {
                System.out.println(dest + " is not reachable");
            } else {
                System.out.println();
                i = dest;

                do {
                    System.out.print(i + " <-- ");
                    i = path[i];
                } while (i != source);

                System.out.println(i + " = " + dist[dest]);
            }
        }
    }

    private static void fnDijkstra(int[][] cost, int[] dist, int[] path, int[] visited, int source, int dest, int n) {
        int i, j, a, b, min;

        for (i = 0; i < n; i++) {
            visited[i] = 0;
            dist[i] = cost[source][i];
            path[i] = source;
        }

        visited[source] = 1;

        for (i = 1; i < n; i++) {
            min = INF;
            a = -1;

            for (j = 0; j < n; j++) {
                if (visited[j] == 0 && dist[j] < min) {
                    min = dist[j];
                    a = j;
                }
            }

            if (a == -1) {
                return;
            }

            visited[a] = 1;

            if (a == dest) {
                return;
            }

            for (b = 0; b < n; b++) {
                if (visited[b] == 0 && dist[a] + cost[a][b] < dist[b]) {
                    dist[b] = dist[a] + cost[a][b];
                    path[b] = a;
                }
            }
        }
    }
}

