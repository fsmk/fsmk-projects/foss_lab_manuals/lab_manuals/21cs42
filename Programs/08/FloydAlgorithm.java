import java.util.Scanner;

public class FloydAlgorithm {
    public static void main(String[] args) {
        int iN, i, j, k;
        int[][] iaFloyd = new int[10][10];
        int[][] iaCost = new int[10][10];

        System.out.println("\n*********************************************************");
        System.out.println("*\tPROGRAM TO IMPLEMENT FLOYD'S ALGORITHM\t*");
        System.out.println("*********************************************************");
        System.out.println("\nEnter the number of vertices");
        Scanner scanner = new Scanner(System.in);
        iN = scanner.nextInt();

        System.out.println("\nEnter the Cost adjacency Matrix");
        for (i = 0; i < iN; i++) {
            for (j = 0; j < iN; j++) {
                iaCost[i][j] = scanner.nextInt();
            }
        }
        System.out.println("\nInput Graph");
        for (i = 0; i < iN; i++) {
            for (j = 0; j < iN; j++) {
                System.out.print(iaCost[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();

        for (i = 0; i < iN; i++) {
            for (j = 0; j < iN; j++) {
                iaFloyd[i][j] = iaCost[i][j];
            }
        }

        for (k = 0; k < iN; k++) {
            for (i = 0; i < iN; i++) {
                for (j = 0; j < iN; j++) {
                    if (iaFloyd[i][j] > (iaFloyd[i][k] + iaFloyd[k][j]))
                        iaFloyd[i][j] = (iaFloyd[i][k] + iaFloyd[k][j]);
                }
            }
        }

        System.out.println("\nAll Pair Shortest Path Matrix");
        for (i = 0; i < iN; i++) {
            for (j = 0; j < iN; j++) {
                System.out.print(iaFloyd[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}

