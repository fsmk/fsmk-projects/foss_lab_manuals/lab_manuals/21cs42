/******************************************************************************
*File		: 01SelectionSort.cpp
*Description	: Program to sort an array using Merge Sort
*Author: Prabodh C P
*Compiler: gcc compiler 7.5.0, Ubuntu 18.04
*Date: Friday 4 February 2020
******************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <ctime>
#include <cstdlib>
using namespace std;

class SelectionSort{
    vector <int> numList;
    int iNum;
    public:

    void fnGenRandArray(int);
    void fnSortArray(int);
    void fnDispArray(int);
    void fnSwap(int &,int &);
};

int main( int argc, char **argv)
{

    struct timespec tv;
    int iChoice, i, iNum;
    double dStart, dEnd;
    SelectionSort myListObj;
	ofstream fout("SelectPlot.dat", ios::out);
    for(;;)
    {
        cout << "\n1.SelectionSort\n2.Plot the Graph\n3.Exit" ;
        cout << "\nEnter your choice\n";
        cin >> iChoice;
        switch(iChoice)
        {
            case 1:
                    cout << "\nEnter number of elements to sort : "; cin >> iNum;
                    myListObj.fnGenRandArray(iNum);
                    cout << "\nUnsorted Array" << endl;
                    myListObj.fnDispArray(iNum);
                    myListObj.fnSortArray(iNum);
                    cout << "\nSorted Array" << endl;
                    myListObj.fnDispArray(iNum);
                    break;
            case 2: for(i=100;i<10000;i+=100)
                    {
                        myListObj.fnGenRandArray(i);
                        clock_gettime(CLOCK_REALTIME, &tv);
                        dStart = tv.tv_sec + tv.tv_nsec/1000000000.0;
                        myListObj.fnSortArray(i);
                        clock_gettime(CLOCK_REALTIME, &tv);
                        dEnd = tv.tv_sec + tv.tv_nsec/1000000000.0;

                        fout << i << "\t" << setprecision(10) << dEnd - dStart << endl;
                    }
                    cout << "\nData File generated and stored in file < SelectPlot.dat >.\n Use a plotting utility\n";
                    break;
            case 3:
                    exit(0);
        }
    }
    fout.close();
    return 0;
}

void SelectionSort::fnGenRandArray(int n)
{
    int i, iVal;

	srand(time(NULL));
	for(i=0;i<n;i++)
	{
        iVal = rand()%10000;
        numList.push_back(iVal);
	}
}

void SelectionSort::fnDispArray(int n)
{
    int i;
	for(i=0;i<n;i++)
	{
        cout << setw(8) << numList[i] << endl;
    }
}


void SelectionSort::fnSortArray(int n)
{
	int i, j, min_idx;
	for(i=0;i<n-1;i++)
	{
		min_idx = i;
		for(j=i+1;j<n;j++)
		{
			if(numList[j] < numList[min_idx])
				min_idx = j;
		}
		fnSwap(numList[i], numList[min_idx]);
	}
}

void SelectionSort::fnSwap(int &a,int &b)
{
	int t;
	t = a;
	a = b;
	b = t;
}

