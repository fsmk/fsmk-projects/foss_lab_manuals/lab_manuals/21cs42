import java.util.Scanner;

class HamiltonianCycle {
    private static final int V = 5;

    
    private static boolean isSafe(int v, int[][] graph, int[] path, int pos) {

        if (graph[path[pos - 1]][v] == 0)
            return false;

        for (int i = 0; i < pos; i++) {
            if (path[i] == v)
                return false;
        }

        return true;
    }


    private static boolean hamCycleUtil(int[][] graph, int[] path, int pos) {

        if (pos == V) {
            if (graph[path[pos - 1]][path[0]] == 1)
                return true;
            else
                return false;
        }

        for (int v = 1; v < V; v++) {
            if (isSafe(v, graph, path, pos)) {
                path[pos] = v;

                if (hamCycleUtil(graph, path, pos + 1) == true)
                    return true;

                path[pos] = -1;
            }
        }

        return false;
    }

    private static boolean findHamCycle(int[][] graph) {
        int[] path = new int[V];
        for (int i = 0; i < V; i++)
            path[i] = -1;

        path[0] = 0;
        if (hamCycleUtil(graph, path, 1) == false) {
            System.out.println("\nSolution does not exist for this graph");
            return false;
        }

        printSolution(path);
        return true;
    }

    private static void printSolution(int[] path) {
        System.out.print("Solution Exists: Following is one Hamiltonian Cycle \n");
        for (int i = 0; i < V; i++)
            System.out.print(path[i] + " ");

        System.out.print(path[0] + " ");
        System.out.println();
    }

    public static void main(String[] args) {
        int iNum;
        int[][] g1 = new int[V][V];
        int[][] g2 = new int[V][V];

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of nodes in the first graph: ");
        iNum = scanner.nextInt();
        System.out.println("Enter the adjacency matrix of the first graph:");
        for (int i = 0; i < iNum; i++) {
            for (int j = 0; j < iNum; j++) {
                g1[i][j] = scanner.nextInt();
            }
        }

        findHamCycle(g1);

        System.out.print("Enter the number of nodes in the second graph: ");
        iNum = scanner.nextInt();
        System.out.println("Enter the adjacency matrix of the second graph:");
        for (int i = 0; i < iNum; i++) {
            for (int j = 0; j < iNum; j++) {
                g2[i][j] = scanner.nextInt();
            }
        }

        findHamCycle(g2);
    }
}

