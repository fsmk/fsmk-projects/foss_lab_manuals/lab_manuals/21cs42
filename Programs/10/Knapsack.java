import java.util.Scanner;

class Knapsack {
    private int totalProfit;
    private int[] weight;
    private int[] profit;
    private int capacity;
    private int numOfObj;
    private int[] subset;
    private int[][] table;

    public Knapsack() {
        totalProfit = 0;
    }

    public void fnReadKnapDetails() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the maximum number of objects: ");
        numOfObj = scanner.nextInt();

        weight = new int[numOfObj + 1];
        profit = new int[numOfObj + 1];

        System.out.println("\nEnter the weights:");
        for (int i = 1; i <= numOfObj; i++) {
            System.out.print("Weight " + i + ": ");
            weight[i] = scanner.nextInt();
        }

        System.out.println("\nEnter the profits:");
        for (int i = 1; i <= numOfObj; i++) {
            System.out.print("Profit " + i + ": ");
            profit[i] = scanner.nextInt();
        }

        System.out.print("\nEnter the maximum capacity: ");
        capacity = scanner.nextInt();

        subset = new int[numOfObj + 1];
        table = new int[numOfObj + 1][capacity + 1];
    }

    private int max(int x, int y) {
        return (x > y) ? x : y;
    }

    public void fnCalcProfit() {
        for (int j = 0; j <= capacity; j++)
            table[0][j] = 0;

        for (int i = 0; i <= numOfObj; i++)
            table[i][0] = 0;

        for (int i = 1; i <= numOfObj; i++) {
            for (int j = 1; j <= capacity; j++) {
                if (j - weight[i] < 0)
                    table[i][j] = table[i - 1][j];
                else
                    table[i][j] = max(table[i - 1][j], profit[i] + table[i - 1][j - weight[i]]);
            }
        }

        totalProfit = table[numOfObj][capacity];
    }

    public void fnFindSubSet() {
        int i = numOfObj;
        int j = capacity;

        while (i >= 1 && j >= 1) {
            if (table[i][j] != table[i - 1][j]) {
                subset[i] = 1;
                j = j - weight[i];
            }
            i--;
        }

        System.out.print("Items selected for the Knapsack are: ");
        for (i = 1; i <= numOfObj; i++) {
            if (subset[i] == 1)
                System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("Total Profit earned is " + totalProfit);
    }

    public static void main(String[] args) {
        Knapsack knapsackObj = new Knapsack();

        knapsackObj.fnReadKnapDetails();
        knapsackObj.fnCalcProfit();
        knapsackObj.fnFindSubSet();
    }
}

