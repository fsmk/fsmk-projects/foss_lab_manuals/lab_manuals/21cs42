#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <ctime>
#include <cstdlib>
using namespace std;


typedef struct{
    int profit;
    int weight;
    float profitRatio;
    float fraction;
}Item;
int main()
{
    vector <Item> itemList;
    int iNum, i, pos;
    float knCap, remCap, totProfit;
    Item a;

    cout << "Enter the number of items : " ;
    cin >> iNum;

    cout << "Enter Knapsack capacity : ";
    cin >> knCap;

    for(i=0;i<iNum;i++)
    {

        cout << "Enter profit : "; cin >> a.profit;
        cout << "Enter weight : "; cin >> a.weight;
        a.profitRatio = (float)a.profit / a.weight;
        a.fraction = 0.0f;
        //cout << a.profitRatio << endl;
        if (itemList.size() == 0)
        {
            itemList.push_back(a);
        }
        else
        {
            pos=0;
            while(a.profitRatio < itemList[pos].profitRatio) pos++;
            itemList.insert(itemList.begin() + pos,a);
        }
    }

    remCap = knCap;
    totProfit = 0.0;
    for(i=0;i<iNum;i++)
    {
        a = itemList[i];
        if(a.weight < remCap)
        {
            itemList[i].fraction = 1.0;
            remCap -= itemList[i].weight;
            totProfit += itemList[i].profit;
        }
        else
        {
            itemList[i].fraction = remCap / itemList[i].weight;
            remCap -= itemList[i].weight * itemList[i].fraction;
            totProfit += itemList[i].profit * itemList[i].fraction;
        }
        if(remCap == 0)
            break;
    }
    cout << "\nKNAPSACK SOLUTION\n";
    cout << "Item\tWeight\tProfit\tFraction Chosen\n";
    for(i=0;i<iNum;i++)
    {
        cout << i+1 << "\t" << itemList[i].weight << "\t" << itemList[i].profit << "\t" << setprecision(2) << itemList[i].fraction << endl;
    }
    cout  << "\nTotal Profit Earned : " << totProfit << endl;

    return 0;
}

