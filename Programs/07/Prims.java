import java.util.Scanner;

class Prims {
    static final int MAXNODES = 10;

    static void fnPrims(int n, int[][] cost) {
        int i, j, u, v, min;
        int sum, k, t[][] = new int[MAXNODES][2], p[] = new int[MAXNODES], d[] = new int[MAXNODES], s[] = new int[MAXNODES];
        int source, count;

        min = 9999;
        source = 0;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (cost[i][j] != 0 && cost[i][j] <= min) {
                    min = cost[i][j];
                    source = i;
                }
            }
        }
        for (i = 0; i < n; i++) {
            d[i] = cost[source][i];
            s[i] = 0;
            p[i] = source;
        }
        s[source] = 1;
        sum = 0;
        k = 0;
        count = 0;
        while (count != n - 1) {
            min = 9999;
            u = -1;
            for (j = 0; j < n; j++) {
                if (s[j] == 0) {
                    if (d[j] <= min) {
                        min = d[j];
                        u = j;
                    }
                }
            }
            t[k][0] = u;
            t[k][1] = p[u];
            k++;
            count++;
            sum += cost[u][p[u]];
            s[u] = 1;
            for (v = 0; v < n; v++) {
                if (s[v] == 0 && cost[u][v] < d[v]) {
                    d[v] = cost[u][v];
                    p[v] = u;
                }
            }
        }
        if (sum >= 9999)
            System.out.println("\nSpanning tree does not exist");
        else {
            System.out.println("\nThe spanning tree exists and the minimum cost spanning tree is:");
            for (i = 0; i < k; i++)
                System.out.println(t[i][1] + " " + t[i][0]);
            System.out.println("\nThe cost of the minimum cost spanning tree is " + sum);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a[][] = { { 0, 3, 9999, 7, 9 }, { 3, 0, 4, 2, 9999 }, { 9999, 4, 0, 5, 6 }, { 7, 2, 5, 0, 4 },
                { 9, 9999, 6, 4, 0 } };
        int n = 5;
        System.out.print("Enter the number of vertices: ");
        n = scanner.nextInt();
        fnGetMatrix(n, a);
        fnPrims(n, a);
    }

    static void fnGetMatrix(int n, int a[][]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Cost Adjacency Matrix:");
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                a[i][j] = scanner.nextInt();
        scanner.close();
    }
}

