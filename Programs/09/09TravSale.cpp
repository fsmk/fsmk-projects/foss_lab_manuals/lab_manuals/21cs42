#include<iostream>
#include<iomanip>
 
using namespace std;
 
int cities[10][10],completed[10],n,cost=0;
 
void takeInput();
int least(int );
void mincost(int);
 
int main()
{
	takeInput();
	 
	cout << "\n\nThe Path is:\n";
	mincost(0); //passing 0 because starting vertex
	 
	cout << "\n\nMinimum cost is " << cost << endl;
	 
	return 0;
}

void takeInput()
{
	int i,j;
	 
	cout << "Enter the number of cities: ";
	cin >> n;
	 
	cout << "\nEnter the Cost Matrix\n";
	 
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
			cin >> cities[i][j];		 
		completed[i]=0;
	}
	 
	cout << "\n\nThe cost list is:";
	 
	for( i=0;i < n;i++)
	{
		cout << endl;
		for(j=0;j < n;j++)
			cout << "\t" << cities[i][j];
	}
}
 
int least(int c)
{
	int i,nc=999;
	int min=999,kmin;
	 
	for(i=0;i < n;i++)
	{
		if((cities[c][i]!=0)&&(completed[i]==0))
		{
			if(cities[c][i]+cities[i][c] < min)
			{
				min=cities[i][0]+cities[c][i];
				kmin=cities[c][i];
				nc=i;
			}		
		}
	}	 
	if(min!=999)
		cost+=kmin;
	 
	return nc;
}
 
void mincost(int city)
{
	int ncity;
	 
	completed[city]=1;
	 
	cout << city << "--->";
	ncity=least(city);
	 
	if(ncity==999)
	{
		ncity=0;
		cout << ncity;
		cost+=cities[city][ncity];
		 
		return;
	}
	 
	mincost(ncity);
}
 

