import java.util.Scanner;

public class TravelingSalesmanProblem {
    private static int[][] cities;
    private static int[] completed;
    private static int n;
    private static int cost;

    public static void main(String[] args) {
        takeInput();

        System.out.println("\n\nThe Path is:");
        minCost(0); // Start from vertex 0

        System.out.println("\n\nMinimum cost is " + cost);
    }

    private static void takeInput() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of cities: ");
        n = scanner.nextInt();

        cities = new int[n][n];
        completed = new int[n];

        System.out.println("\nEnter the Cost Matrix");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                cities[i][j] = scanner.nextInt();

            completed[i] = 0;
        }

        System.out.println("\n\nThe cost list is:");

        for (int i = 0; i < n; i++) {
            System.out.println();
            for (int j = 0; j < n; j++)
                System.out.print("\t" + cities[i][j]);
        }
    }

    private static int least(int c) {
        int nc = 999;
        int min = 999;
        int kmin = 0;

        for (int i = 0; i < n; i++) {
            if (cities[c][i] != 0 && completed[i] == 0) {
                if (cities[c][i] + cities[i][c] < min) {
                    min = cities[i][0] + cities[c][i];
                    kmin = cities[c][i];
                    nc = i;
                }
            }
        }

        if (min != 999)
            cost += kmin;

        return nc;
    }

    private static void minCost(int city) {
        completed[city] = 1;

        System.out.print(city + "--->");

        int ncity = least(city);

        if (ncity == 999) {
            ncity = 0;
            System.out.print(ncity);
            cost += cities[city][ncity];
            return;
        }

        minCost(ncity);
    }
}

