import java.util.Scanner;

class SubSet {
    private static final int MAX = 100;
    private int[] stk = new int[MAX];
    private int[] set = new int[MAX];
    private int size;
    private int top;
    private int count;
    private static int foundSoln = 0;

    public SubSet() {
        top = -1;
        count = 0;
    }

    public void getInfo() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the maximum number of elements: ");
        size = scanner.nextInt();
        System.out.println("Enter the values of the elements: ");
        for (int i = 1; i <= size; i++) {
            set[i] = scanner.nextInt();
        }
    }

    public void push(int data) {
        stk[++top] = data;
    }

    public void pop() {
        top--;
    }

    public void display() {
        System.out.print("\nSOLUTION #" + (++count) + " IS\n{ ");
        for (int i = 0; i <= top; i++) {
            System.out.print(stk[i] + " ");
        }
        System.out.println("}");
    }

    public int findSubset(int pos, int sum) {

        if (sum > 0) {
            for (int i = pos; i <= size; i++) {
                push(set[i]);
                foundSoln = findSubset(i + 1, sum - set[i]);
                pop();
            }
        }
        if (sum == 0) {
            display();
            foundSoln = 1;
        }
        return foundSoln;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sum;
        SubSet set1 = new SubSet();
        set1.getInfo();
        System.out.print("Enter the sum value: ");
        sum = scanner.nextInt();
        System.out.println();

        if (set1.findSubset(1, sum) == 0) {
            System.out.println("The problem instance doesn't have any solution.");
        } else {
            System.out.println("The above-mentioned sets are the required solution to the given instance.");
        }
        scanner.close();
    }
}

